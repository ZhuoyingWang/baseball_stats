class CreatePitchings < ActiveRecord::Migration
  def change
    create_table :pitchings do |t|
      t.string :throws
      t.integer :wins
      t.integer :losses
      t.integer :saves
      t.integer :complete_games
      t.integer :shut_outs
      t.integer :era
      t.integer :innings
      t.integer :earned_runs
      t.integer :hit_batter
      t.integer :wild_pitches
      t.integer :balk
      t.integer :walked_batter
      t.integer :struck_out_batter

      t.timestamps null: false
    end
  end
end
