class CreateDivisions < ActiveRecord::Migration
  def change
    create_table :divisions do |t|
      t.string :division_name
      t.references :league, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
