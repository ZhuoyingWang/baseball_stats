class CreatePlayerCommons < ActiveRecord::Migration
  def change
    create_table :player_commons do |t|
      t.string :surname
      t.string :given_name
      t.integer :games
      t.integer :games_started
      t.integer :runs
      t.integer :home_runs
      t.string :position
      t.string :positionable_type
      t.integer :positionable_id
      t.references :team, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
