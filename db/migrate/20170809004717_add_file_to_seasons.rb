class AddFileToSeasons < ActiveRecord::Migration
  def up
    add_attachment :seasons, :file
  end

  def down
    remove_attachment :seasons, :file
  end
end
