class CreateBattingStats < ActiveRecord::Migration
  def change
    create_table :batting_stats do |t|
      t.float :avg
      t.integer :home_runs
      t.integer :rbi
      t.integer :runs
      t.integer :sb
      t.float :ops
      t.references :batting, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
