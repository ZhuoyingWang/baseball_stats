# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170812180426) do

  create_table "batting_stats", force: :cascade do |t|
    t.float    "avg"
    t.integer  "home_runs"
    t.integer  "rbi"
    t.integer  "runs"
    t.integer  "sb"
    t.float    "ops"
    t.integer  "batting_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "batting_stats", ["batting_id"], name: "index_batting_stats_on_batting_id"

  create_table "battings", force: :cascade do |t|
    t.integer  "at_bats"
    t.integer  "hits"
    t.integer  "doubles"
    t.integer  "triples"
    t.integer  "rbi"
    t.integer  "steals"
    t.integer  "caught_stealing"
    t.integer  "sacrifice_hits"
    t.integer  "sacrifice_flies"
    t.integer  "base_errors"
    t.integer  "pb"
    t.integer  "walks"
    t.integer  "struck_out"
    t.integer  "hit_by_pitch"
    t.integer  "batting_stat_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "battings", ["batting_stat_id"], name: "index_battings_on_batting_stat_id"

  create_table "divisions", force: :cascade do |t|
    t.string   "division_name"
    t.integer  "league_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "divisions", ["league_id"], name: "index_divisions_on_league_id"

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"

  create_table "leagues", force: :cascade do |t|
    t.string   "league_name"
    t.integer  "season_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "leagues", ["season_id"], name: "index_leagues_on_season_id"

  create_table "pitchings", force: :cascade do |t|
    t.string   "throws"
    t.integer  "wins"
    t.integer  "losses"
    t.integer  "saves"
    t.integer  "complete_games"
    t.integer  "shut_outs"
    t.integer  "era"
    t.integer  "innings"
    t.integer  "earned_runs"
    t.integer  "hit_batter"
    t.integer  "wild_pitches"
    t.integer  "balk"
    t.integer  "walked_batter"
    t.integer  "struck_out_batter"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "player_commons", force: :cascade do |t|
    t.string   "surname"
    t.string   "given_name"
    t.integer  "games"
    t.integer  "games_started"
    t.integer  "runs"
    t.integer  "home_runs"
    t.string   "position"
    t.string   "positionable_type"
    t.integer  "positionable_id"
    t.integer  "team_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "player_commons", ["team_id"], name: "index_player_commons_on_team_id"

  create_table "seasons", force: :cascade do |t|
    t.integer  "year"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "teams", force: :cascade do |t|
    t.string   "team_city"
    t.string   "team_name"
    t.integer  "division_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "teams", ["division_id"], name: "index_teams_on_division_id"

end
