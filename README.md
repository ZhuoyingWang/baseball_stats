== README

This demo app includes few functionalities, such as upload xml file in certain format, display hitter stats, database management.

### Gems
The Gems used in this demo app for different purposes are listed below.

##### Rails
 	gem 'rails', '4.2.7'
##### Debug
	gem "byebug"
	gem 'pry-byebug'

##### Deployment
  	gem 'capistrano',"~> 3.7.2",require: false
  	gem 'capistrano-rvm',     require: false
  	gem 'capistrano-rails',   require: false
  	gem 'capistrano-bundler', require: false
  	gem 'capistrano3-puma',   require: false
  	gem 'capistrano-rbenv'
  	gem 'puma'
  	gem "pg"

##### Database Management
  	gem "rails -db"

##### Data Parser
  	gem "Nokogiri"

##### Front end
  	gem "bootstrap-sass"
  	gem 'sass-rails'
    
##### File upload
  	gem "paperclip"
    
##### Pagnation
  	gem "will_paginate"


## Server Side Configuration
###### system: ubuntu 16.04 on digtialocean
###### database: postgresql
###### editor: vim
###### webserver: nginx
###### appserver: puma
###### ruby_manager: rbenv
###### variable_holder: rbenv-vars

## Databse Design
use polymophic association
![](./images/Baseball_database.png)


## Usage
##### Upload xml file
Use [this link](http://45.55.180.67/seasons/new) to upload file, this maybe take a while, please be patient.
##### Xml file Information page
After uploading, use [this link](http://45.55.180.67/seasons/1) to Xml file information page.
##### Database Page
After uploading, use [this link](http://45.55.180.67/batting_stats) to database page.
##### Baseball stats page
After uploading, use [this link](http://45.55.180.67/rails/db) to baseball stats page.





