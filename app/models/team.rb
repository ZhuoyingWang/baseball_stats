class Team < ActiveRecord::Base
  belongs_to :division
  has_many :player_commons
end
