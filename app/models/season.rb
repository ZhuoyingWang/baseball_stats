class Season < ActiveRecord::Base
  has_many :leagues
  has_many :divisions, :through => :leagues
  has_attached_file :file
  validates_attachment :file, content_type: {content_type: "text/xml"}
  before_save :parse_file
  # before_validation :parse_file

  def parse_file
    tempfile = file.queued_for_write[:original]
    doc = Nokogiri::XML(tempfile)
    parse_xml(doc)
  end

  def parse_xml(doc)
    # parse seasons
    doc.root.elements.each do |node|
      if node.node_name.eql? "YEAR"
        self.year = node.text.to_i
      elsif node.node_name.eql? "LEAGUE"
        tmp_league = League.new
        parse_leagues(node, tmp_league)
        self.leagues << tmp_league
      end
    end
  end

  def parse_leagues(node, tmp_league)
    # parse Leagues
    node.elements.each do |node|
      # tmp_league.league_name = node.text.to_s if node.name.eql? "LEAGUE_NAME"
      if node.name.eql? "LEAGUE_NAME"
        tmp_league.league_name = node.text.to_s
      elsif node.name.eql? "DIVISION"
        tmp_division = Division.new
        parse_divisions(node, tmp_division)
        tmp_league.divisions << tmp_division
      end
    end
    tmp_league
  end

  def parse_divisions(node, tmp_division)
    node.elements.each do |node|
      # puts node.node_name
      # tmp_division.division_name = node.text.to_s if node.name.eql? "DIVISION_NAME"
      if node.name.eql? "DIVISION_NAME"
        tmp_division.division_name = node.text.to_s
      elsif node.name.eql? "TEAM"
        tmp_team = Team.new
        parse_teams(node, tmp_team)
        tmp_division.teams << tmp_team
      end
    end
    tmp_division
  end

  def parse_teams(node, tmp_team)
    node.elements.each do |node|
      if node.name.eql? "TEAM_CITY"
        tmp_team.team_city = node.text.to_s
      elsif node.name.eql? "TEAM_NAME"
        tmp_team.team_name = node.text.to_s
      elsif node.name.eql? "PLAYER"
        tmp_player = PlayerCommon.new
        parse_players(node, tmp_player)
        tmp_team.player_commons << tmp_player
      end
    end
    tmp_team
  end

  def parse_players(node, tmp_player)
    if node.elements[2].name == "POSITION"
      parse_base_type(node, tmp_player)
    else
      parse_pitcher_type(node, tmp_player)
    end
    # unless node.elements[2].name == "POSITION"
    #   parse_pitcher_type(node, tmp_player)
    # end
    tmp_player
  end

  def parse_player_share(node, tmp_player)
    node.elements.each do |node|
      # tmp_division.division_name = node.text.to_s if node.name.eql? "DIVISION_NAME"
      tmp_player.surname = node.text.to_s if node.name.eql? "SURNAME"
      tmp_player.given_name = node.text.to_s if node.name.eql? "GIVEN_NAME"
      tmp_player.games = node.text.to_s if node.name.eql? "GAMES"
      tmp_player.games_started = node.text.to_s if node.name.eql? "GAMES_STARTED"
      tmp_player.runs = node.text.to_s if node.name.eql? "RUNS"
      tmp_player.home_runs = node.text.to_s if node.name.eql? "HOME_RUNS"
      tmp_player.position = node.text.to_s if node.name.eql? "POSITION"
    end
    tmp_player
  end

  def parse_base_type(node, tmp_player)
    parse_player_share(node, tmp_player)
    base_position = ["AT_BATS", "HITS", "DOUBLES", "TRIPLES", "RBI", "STEALS", "CAUGHT_STEALING", "SACRIFICE_HITS", "SACRIFICE_FLIES", "ERRORS", "PB", "WALKS", "STRUCK_OUT", "HIT_BY_PITCH"]
    tmp_base = Batting.new
    node.elements.each do |node|
      tmp_base.at_bats = node.text.to_i if node.name.eql? "AT_BATS"
      tmp_base.hits = node.text.to_i if node.name.eql? "HITS"
      tmp_base.doubles = node.text.to_i if node.name.eql? "DOUBLES"
      tmp_base.triples = node.text.to_i if node.name.eql? "TRIPLES"
      tmp_base.rbi = node.text.to_i if node.name.eql? "RBI"
      tmp_base.steals = node.text.to_i if node.name.eql? "STEALS"
      tmp_base.caught_stealing = node.text.to_i if node.name.eql? "CAUGHT_STEALING"
      tmp_base.sacrifice_hits = node.text.to_i if node.name.eql? "SACRIFICE_HITS"
      tmp_base.sacrifice_flies = node.text.to_i if node.name.eql? "SACRIFICE_FLIES"
      tmp_base.base_errors = node.text.to_i if node.name.eql? "ERRORS"
      tmp_base.pb = node.text.to_i if node.name.eql? "PB"
      tmp_base.walks = node.text.to_i if node.name.eql? "WALKS"
      tmp_base.struck_out = node.text.to_i if node.name.eql? "STRUCK_OUT"
      tmp_base.hit_by_pitch = node.text.to_i if node.name.eql? "HIT_BY_PITCH"
    end
    tmp_player.positionable = tmp_base
    # binding.pry
    parse_stat(tmp_base, tmp_player)
    tmp_base.save

    tmp_player
  end

  def parse_pitcher_type(node, tmp_player)
    parse_player_share(node, tmp_player)
    pitcher_position = ["THROWS", "WINS", "LOSSES", "SAVES", "COMPLETE_GAMES", "SHUT_OUTS", "ERA", "INNINGS", "EARNED_RUNS", "HIT_BATTER", "WILD_PITCHES", "BALK", "WALKED_BATTER", "STRUCK_OUT_BATTER"]
    tmp_pitcher = Pitching.new
    # tmp_player.positionable_id = tmp_pitcher.id
    node.elements.each do |node|
      tmp_pitcher.throws = node.text.to_s if node.name.eql? "THROWS"
      tmp_pitcher.wins = node.text.to_i if node.name.eql? "WINS"
      tmp_pitcher.losses = node.text.to_i if node.name.eql? "LOSSES"
      tmp_pitcher.saves = node.text.to_i if node.name.eql? "SAVES"
      tmp_pitcher.complete_games = node.text.to_i if node.name.eql? "COMPLETE_GAMES"
      tmp_pitcher.shut_outs = node.text.to_i if node.name.eql? "SHUT_OUTS"
      tmp_pitcher.era = node.text.to_i if node.name.eql? "ERA"
      tmp_pitcher.innings = node.text.to_i if node.name.eql? "INNINGS"
      tmp_pitcher.earned_runs = node.text.to_i if node.name.eql? "EARNED_RUNS"
      tmp_pitcher.hit_batter = node.text.to_i if node.name.eql? "HIT_BATTER"
      tmp_pitcher.wild_pitches = node.text.to_i if node.name.eql? "WILD_PITCHES"
      tmp_pitcher.balk = node.text.to_i if node.name.eql? "BALK"
      tmp_pitcher.walked_batter = node.text.to_i if node.name.eql? "WALKED_BATTER"
      tmp_pitcher.struck_out_batter = node.text.to_i if node.name.eql? "STRUCK_OUT_BATTER"
    end
    # binding.pry
    tmp_pitcher.save
    # tmp_pitcher.player_common = tmp_player
    # tmp_pitcher.player_common = tmp_player
    tmp_player.positionable = tmp_pitcher
    tmp_player
  end


  def parse_stat(tmp_batting, tmp_player)
    tmp_batting_stat = BattingStat.new
    tmp_batting_stat.avg = avg(tmp_batting)
    tmp_batting_stat.home_runs = tmp_player.home_runs
    tmp_batting_stat.runs = tmp_player.runs
    tmp_batting_stat.rbi = tmp_batting.rbi
    tmp_batting_stat.sb = tmp_batting.steals
    tmp_batting_stat.ops = OPS(tmp_batting, tmp_player, tmp_batting_stat)
    tmp_batting_stat.batting = tmp_batting
    tmp_batting_stat.save
  end

  def avg(tmp_batting)
    tmp_batting.hits.to_f / tmp_batting.at_bats
  end

  def team_name(player)
    player.team.team_name
  end

  def OPS(tmp_batting, tmp_player, tmp_batting_stat)
    SLG(tmp_batting, tmp_player) + OBP(tmp_batting)
  end

  def SLG(tmp_batting, tmp_player)
    tb = TB(tmp_player, tmp_batting)
    tb.to_f/tmp_batting.at_bats
  end

  def TB(tmp_player, tmp_batting)
    # tmp_batting = set_batting(player)
    tb = 1*tmp_batting.hits + 2*tmp_batting.doubles + 3*tmp_batting.triples + 4*tmp_player.home_runs
    # TBS
  end

  def OBP(tmp_batting)
    numerator = numerator(tmp_batting)
    denominator = denominator(tmp_batting)
    numerator.to_f / denominator
  end

  def numerator(tmp_batting)
    hits = tmp_batting.hits
    bb = tmp_batting.walks
    hbp = tmp_batting.hit_by_pitch
    hits + bb + hbp
  end

  def denominator(tmp_batting)
    ab = tmp_batting.at_bats
    bb = tmp_batting.walks
    sf = tmp_batting.sacrifice_flies
    hbp = tmp_batting.hit_by_pitch
    ab + bb + sf + hbp
  end

end
