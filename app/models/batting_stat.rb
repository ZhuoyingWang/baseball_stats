class BattingStat < ActiveRecord::Base
  belongs_to :batting
  scope :has_avg, -> {where.not(avg: "NaN")}
  # has_and_belongs_to_one :batting
  # has_and_belongs_to_many :batting , :limit => 1
  # has_one :player_common, through: :batting, :as => :positionable
end
