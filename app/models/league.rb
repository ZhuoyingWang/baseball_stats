class League < ActiveRecord::Base
  belongs_to :season
  has_many :divisions
  has_many :teams, :through => :divisions
end
