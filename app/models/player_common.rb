class PlayerCommon < ActiveRecord::Base
  belongs_to :team
  belongs_to :positionable, :polymorphic => true
  #belongs_to :batting_stat, through: :batting
end
