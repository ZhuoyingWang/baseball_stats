class Division < ActiveRecord::Base
  belongs_to :league
  has_many :teams
  has_many :player_commons, :through => :teams
end
