class BattingStatsController < ApplicationController
  # helper_method :sort_column, :sort_direction

  def index
    @batting_stats = BattingStat.order("#{sort_column} #{sort_direction}")
                                .has_avg
                                .includes(:batting)
                                .paginate(:page => params[:page], :per_page => 25)
  # .includes(:player_commons)
  # .joins(:batting, :player_common)
  #       # format.html
    respond_to do |format|
      format.html
      format.js
    end
  end

  private
    def sortable_columns
      ["avg", "home_runs", "runs", "rbi", "sb", "ops"]
    end

    def sort_column
      sortable_columns.include?(params[:column]) ? params[:column] : "avg"
    end
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
    end
end
