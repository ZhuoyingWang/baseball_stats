class SeasonsController < ApplicationController
  def index
    @seasons = Season.all
  end

  def show
    @season = Season.find(params[:id])
    # @players = PlayerCommon.paginate(:page => params[:page], :per_page => 25)
  end

  def new
    @season = Season.new
  end

  def create
    @season = Season.new(season_params)
    if @season.save
      flash[:success] = "Great! Your file upload successfully!"
      redirect_to @season
    else
      flash.now[:error]= "Fix your mistakes, please."
      render :new
    end
  end

  def edit
  end

  private

  def season_params
    params.require(:season).permit(:file, :year)
  end
end
