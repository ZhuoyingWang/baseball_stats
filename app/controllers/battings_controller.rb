class BattingsController < ApplicationController
  def index
    @battings = Batting.paginate(:page => params[:page], :per_page => 25)

    respond_to do |format|
      format.html
    end
  end
end
