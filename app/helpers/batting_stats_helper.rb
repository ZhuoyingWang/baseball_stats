module BattingStatsHelper
  def sort_link(column, title=nil)
    title ||= column.titleize
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    icon = sort_direction == "asc" ? "glyphicon glyphicon-chevron-up" : "glyphicon glyphicon-chevron-down"
    icon = column == sort_column ? icon : ""
    link_to "#{title} <span class='#{icon}'></span>".html_safe, {column: column, direction: direction}, remote: true
  end

  def sortable_columns
    ["avg", "home_runs", "runs", "rbi", "sb", "ops"]
  end

  def sort_column
    sortable_columns.include?(params[:column]) ? params[:column] : "avg"
  end
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end

  def set_player(batting_stat)
    batting_stat.batting.player_common
  end

  def get_surname(batting_stat)
    set_player(batting_stat).surname
  end

  def get_given_name(batting_stat)
    set_player(batting_stat).given_name
  end
  def get_position(batting_stat)
    set_player(batting_stat).position
  end
end
